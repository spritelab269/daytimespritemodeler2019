% This file is a part of Daytime Sprite Modeler
% Copyright © 2019 IAP RAS, see LICENSE.txt file for EULA

function dNdt = diff_common(N, v)
    dNdt = [...
        -v(10)*N(18)*N(1)+v(103)*N(38)*N(18)+v(104)*N(38)*N(1)-v(109)*N(40)*N(1)+v(110)*N(40)*N(3)+v(115)*N(28)*N(18)+v(115)*N(28)*N(18)+v(116)*N(28)*N(17)+v(117)*N(28)*N(7)+v(117)*N(28)*N(7)+v(122)*N(14)*N(6)+v(126)*N(11)*N(3)-v(13)*N(18)*N(1)-v(135)*N(32)*N(1)*N(43)-v(136)*N(32)*N(1)-v(137)*N(32)*N(1)+v(139)*N(32)*N(6)-v(14)*N(18)*N(1)+v(140)*N(2)*N(27)+v(142)*N(2)*N(21)+v(143)*N(2)*N(22)+v(144)*N(2)*N(19)+v(144)*N(2)*N(19)+v(145)*N(2)*N(1)-v(146)*N(2)*N(1)*N(1)+v(147)*N(2)*N(3)+v(148)*N(2)*N(6)-v(15)*N(18)*N(1)+v(150)*N(4)*N(9)+v(151)*N(4)*N(21)+v(154)*N(4)*N(7)+v(156)*N(4)*N(6)+v(157)*N(4)*N(6)+v(157)*N(4)*N(6)+v(158)*N(13)*N(9)+v(159)*N(13)*N(43)+v(160)*N(13)*N(7)+v(161)*N(13)*N(1)+v(162)*N(13)*N(6)+v(163)*N(13)*N(6)+v(163)*N(13)*N(6)-v(164)*N(18)*N(27)*N(1)-v(166)*N(18)*N(1)*N(6)+v(168)*N(18)*N(3)-v(170)*N(18)*N(1)*N(1)+v(171)*N(15)*N(41)+v(173)*N(15)*N(3)+v(174)*N(41)*N(41)+v(176)*N(33)*N(21)-v(18)*N(18)*N(1)+v(180)*N(33)*N(3)-v(182)*N(33)*N(1)+v(184)*N(7)*N(3)+v(185)*N(21)*N(22)+v(186)*N(21)*N(3)+v(187)*N(22)*N(22)-v(188)*N(1)*N(15)*N(43)-v(189)*N(1)*N(15)*N(43)-v(19)*N(18)*N(1)+v(190)*N(6)*N(41)-v(191)*N(6)*N(27)*N(1)+v(192)*N(6)*N(21)+v(193)*N(6)*N(22)-v(194)*N(6)*N(1)*N(1)+v(195)*N(6)*N(3)+v(195)*N(6)*N(3)+v(197)*N(6)*N(25)+v(198)*N(6)*N(6)*N(43)-v(2)*N(51)*N(1)+v(20)*N(18)*N(3)+v(201)*N(25)*N(41)+v(202)*N(25)*N(3)+v(204)*N(19)*N(27)+v(205)*N(19)*N(1)+v(206)*N(19)*N(6)+v(207)*N(19)*N(7)+v(208)*N(19)*N(3)+v(208)*N(19)*N(3)+v(210)*N(19)*N(3)+v(210)*N(19)*N(3)-v(211)*N(19)*N(19)*N(1)+v(212)*N(19)+v(213)*N(53)+v(218)*N(53)*N(3)+v(218)*N(53)*N(3)-v(220)*N(54)*N(1)-v(221)*N(54)*N(1)-v(225)*N(54)*N(1)-v(226)*N(54)*N(1)+v(227)*N(54)*N(19)-v(235)*N(55)*N(1)-v(237)*N(56)*N(1)-v(239)*N(58)*N(1)-v(240)*N(58)*N(1)+v(245)*N(59)*N(52)-v(246)*N(59)*N(1)+v(249)*N(59)*N(3)+v(250)*N(59)*N(3)+v(250)*N(59)*N(3)+v(251)*N(59)*N(7)+v(256)*N(60)*N(3)+v(257)*N(60)*N(3)+v(257)*N(60)*N(3)-v(34)*N(23)*N(1)-v(4)*N(51)*N(1)+v(42)*N(26)*N(18)*N(18)+v(43)*N(26)*N(18)*N(43)+v(51)*N(26)*N(7)-v(52)*N(26)*N(1)*N(1)+v(53)*N(44)*N(18)+v(54)*N(44)*N(17)+v(55)*N(44)*N(17)-v(56)*N(36)*N(1)+v(57)*N(36)*N(18)-v(65)*N(31)*N(1)-v(71)*N(35)*N(1)-v(75)*N(34)*N(1)-v(86)*N(39)*N(1)-v(87)*N(39)*N(1)-v(88)*N(39)*N(1)+v(89)*N(39)*N(3)-v(9)*N(18)*N(1)-v(97)*N(20)*N(1)*N(27)-v(98)*N(20)*N(1)*N(1) ;...    % O_2
        +v(120)*N(10)*N(6)+v(136)*N(32)*N(1)-v(140)*N(2)*N(27)-v(141)*N(2)*N(33)-v(142)*N(2)*N(21)-v(143)*N(2)*N(22)-v(144)*N(2)*N(19)-v(145)*N(2)*N(1)-v(146)*N(2)*N(1)*N(1)-v(147)*N(2)*N(3)-v(148)*N(2)*N(6)-v(149)*N(2)*N(6)+v(156)*N(4)*N(6)+v(159)*N(13)*N(43)+v(161)*N(13)*N(1)+v(164)*N(18)*N(27)*N(1)+v(166)*N(18)*N(1)*N(6)+v(169)*N(18)*N(3)+v(170)*N(18)*N(1)*N(1) ;...    % O_2^-
        -v(110)*N(40)*N(3)+v(118)*N(28)*N(6)-v(126)*N(11)*N(3)+v(137)*N(32)*N(1)-v(138)*N(32)*N(3)-v(147)*N(2)*N(3)+v(149)*N(2)*N(6)+v(152)*N(4)*N(21)+v(153)*N(4)*N(22)-v(165)*N(18)*N(1)*N(3)-v(168)*N(18)*N(3)-v(169)*N(18)*N(3)-v(173)*N(15)*N(3)-v(180)*N(33)*N(3)-v(184)*N(7)*N(3)-v(186)*N(21)*N(3)+v(191)*N(6)*N(27)*N(1)+v(194)*N(6)*N(1)*N(1)-v(195)*N(6)*N(3)-v(20)*N(18)*N(3)-v(202)*N(25)*N(3)-v(208)*N(19)*N(3)-v(210)*N(19)*N(3)+v(211)*N(19)*N(19)*N(1)+v(211)*N(19)*N(19)*N(1)-v(218)*N(53)*N(3)-v(249)*N(59)*N(3)-v(250)*N(59)*N(3)-v(256)*N(60)*N(3)-v(257)*N(60)*N(3)+v(49)*N(26)*N(21)-v(66)*N(31)*N(3)-v(89)*N(39)*N(3) ;...    % O_3
        +v(135)*N(32)*N(1)*N(43)+v(138)*N(32)*N(3)+v(147)*N(2)*N(3)-v(150)*N(4)*N(9)-v(151)*N(4)*N(21)-v(152)*N(4)*N(21)-v(153)*N(4)*N(22)-v(154)*N(4)*N(7)-v(155)*N(4)*N(7)-v(156)*N(4)*N(6)-v(157)*N(4)*N(6)+v(162)*N(13)*N(6)+v(165)*N(18)*N(1)*N(3) ;...    % O_3^-
        0 ;...    % CO
        +v(107)*N(40)*N(7)+v(109)*N(40)*N(1)-v(111)*N(40)*N(6)*N(43)+v(113)*N(40)*N(18)*N(18)+v(114)*N(40)*N(18)*N(43)-v(118)*N(28)*N(6)-v(120)*N(10)*N(6)-v(122)*N(14)*N(6)-v(127)*N(11)*N(6)+v(13)*N(18)*N(1)-v(130)*N(16)*N(6)+v(132)*N(32)*N(21)+v(136)*N(32)*N(1)+v(138)*N(32)*N(3)-v(139)*N(32)*N(6)+v(14)*N(18)*N(1)-v(148)*N(2)*N(6)-v(149)*N(2)*N(6)+v(155)*N(4)*N(7)-v(156)*N(4)*N(6)-v(157)*N(4)*N(6)-v(162)*N(13)*N(6)-v(163)*N(13)*N(6)-v(167)*N(18)*N(1)*N(6)+v(169)*N(18)*N(3)+v(177)*N(33)*N(21)+v(177)*N(33)*N(21)+v(179)*N(33)*N(7)+v(18)*N(18)*N(1)+v(18)*N(18)*N(1)-v(181)*N(33)*N(6)*N(43)+v(182)*N(33)*N(1)+v(19)*N(18)*N(1)-v(190)*N(6)*N(41)-v(191)*N(6)*N(27)*N(1)-v(192)*N(6)*N(21)-v(193)*N(6)*N(22)-v(194)*N(6)*N(1)*N(1)-v(195)*N(6)*N(3)-v(196)*N(6)*N(25)*N(43)-v(197)*N(6)*N(25)-v(198)*N(6)*N(6)*N(43)-v(198)*N(6)*N(6)*N(43)+v(20)*N(18)*N(3)+v(203)*N(25)*N(25)+v(208)*N(19)*N(3)+v(209)*N(19)*N(33)+v(218)*N(53)*N(3)-v(219)*N(54)*N(6)+v(220)*N(54)*N(1)+v(221)*N(54)*N(1)+v(221)*N(54)*N(1)-v(230)*N(54)*N(6)+v(235)*N(55)*N(1)+v(235)*N(55)*N(1)+v(237)*N(56)*N(1)+v(239)*N(58)*N(1)+v(241)*N(58)*N(7)-v(243)*N(58)*N(6)+v(246)*N(59)*N(1)+v(247)*N(59)*N(27)+v(248)*N(59)*N(1)+v(249)*N(59)*N(3)+v(249)*N(59)*N(3)+v(252)*N(59)+v(254)*N(60)*N(27)+v(256)*N(60)*N(3)+v(258)*N(60)*N(7)+v(260)*N(60)*N(52)+v(263)*N(60)*N(19)+v(263)*N(60)*N(19)+v(263)*N(60)*N(19)+v(4)*N(51)*N(1)+v(44)*N(26)*N(18)+v(44)*N(26)*N(18)+v(50)*N(26)*N(33)+v(66)*N(31)*N(3)-v(67)*N(31)*N(6)-v(68)*N(31)*N(6)-v(76)*N(34)*N(6)-v(77)*N(39)*N(6)+v(83)*N(39)*N(7)-v(85)*N(39)*N(6)*N(43)+v(87)*N(39)*N(1)+v(93)*N(20)*N(18) ;...    % O
        +v(100)*N(37)*N(18)+v(103)*N(38)*N(18)-v(107)*N(40)*N(7)-v(108)*N(40)*N(7)-v(117)*N(28)*N(7)-v(119)*N(10)*N(7)-v(121)*N(14)*N(7)+v(123)*N(11)*N(15)+v(124)*N(11)*N(21)-v(128)*N(8)*N(7)+v(131)*N(32)*N(33)-v(133)*N(32)*N(7)*N(43)-v(134)*N(32)*N(7)-v(154)*N(4)*N(7)-v(155)*N(4)*N(7)-v(16)*N(18)*N(7)-v(160)*N(13)*N(7)+v(178)*N(33)*N(21)+v(178)*N(33)*N(21)-v(179)*N(33)*N(7)+v(180)*N(33)*N(3)+v(181)*N(33)*N(6)*N(43)+v(182)*N(33)*N(1)-v(183)*N(7)*N(22)-v(184)*N(7)*N(3)+v(185)*N(21)*N(22)+v(192)*N(6)*N(21)+v(209)*N(19)*N(33)+v(219)*N(54)*N(6)+v(22)*N(50)*N(18)+v(222)*N(54)*N(52)+v(238)*N(58)*N(52)+v(239)*N(58)*N(1)+v(24)*N(48)*N(18)+v(240)*N(58)*N(1)-v(241)*N(58)*N(7)+v(244)*N(59)*N(52)+v(244)*N(59)*N(52)-v(251)*N(59)*N(7)+v(26)*N(49)*N(18)+v(27)*N(49)*N(17)+v(30)*N(47)*N(18)+v(48)*N(26)*N(27)-v(51)*N(26)*N(7)-v(64)*N(31)*N(7)+v(70)*N(35)*N(21)-v(74)*N(34)*N(7)-v(82)*N(39)*N(7)-v(83)*N(39)*N(7)-v(84)*N(39)*N(7)+v(88)*N(39)*N(1)+v(91)*N(20)*N(18)*N(18)+v(92)*N(20)*N(18)*N(43)+v(95)*N(20)*N(11)+v(96)*N(20)*N(8) ;...    % NO
        +v(121)*N(14)*N(7)+v(124)*N(11)*N(21)+v(125)*N(11)*N(22)+v(126)*N(11)*N(3)-v(128)*N(8)*N(7)+v(143)*N(2)*N(22)+v(151)*N(4)*N(21)+v(153)*N(4)*N(22)+v(155)*N(4)*N(7)+v(160)*N(13)*N(7)-v(96)*N(20)*N(8) ;...    % NO_3^-
        +v(119)*N(10)*N(7)+v(120)*N(10)*N(6)+v(121)*N(14)*N(7)-v(150)*N(4)*N(9)-v(158)*N(13)*N(9)+v(22)*N(50)*N(18)+v(23)*N(50)*N(17)-v(90)*N(20)*N(9)*N(43) ;...    % CO_2
        -v(119)*N(10)*N(7)-v(120)*N(10)*N(6)+v(122)*N(14)*N(6)+v(150)*N(4)*N(9) ;...    % CO_3^-
        +v(119)*N(10)*N(7)-v(123)*N(11)*N(15)-v(124)*N(11)*N(21)-v(125)*N(11)*N(22)-v(126)*N(11)*N(3)-v(127)*N(11)*N(6)+v(128)*N(8)*N(7)+v(132)*N(32)*N(21)+v(133)*N(32)*N(7)*N(43)+v(142)*N(2)*N(21)+v(152)*N(4)*N(21)+v(154)*N(4)*N(7)-v(95)*N(20)*N(11) ;...    % NO_2^-
        0 ;...    % NO^-
        +v(146)*N(2)*N(1)*N(1)-v(158)*N(13)*N(9)-v(159)*N(13)*N(43)-v(160)*N(13)*N(7)-v(161)*N(13)*N(1)-v(162)*N(13)*N(6)-v(163)*N(13)*N(6) ;...    % O_4^-
        -v(121)*N(14)*N(7)-v(122)*N(14)*N(6)+v(158)*N(13)*N(9) ;...    % CO_4^-
        -v(123)*N(11)*N(15)-v(129)*N(16)*N(15)-v(171)*N(15)*N(41)-v(172)*N(15)*N(41)-v(173)*N(15)*N(3)-v(188)*N(1)*N(15)*N(43)-v(189)*N(1)*N(15)*N(43)+v(197)*N(6)*N(25)-v(199)*N(25)*N(15)*N(27)-v(200)*N(25)*N(15)*N(1)+v(35)*N(24)*N(18)+v(37)*N(30)*N(18)+v(39)*N(45)*N(18)+v(41)*N(46)*N(18) ;...    % H
        +v(123)*N(11)*N(15)-v(129)*N(16)*N(15)-v(130)*N(16)*N(6) ;...    % OH^-
        -v(101)*N(37)*N(17)-v(116)*N(28)*N(17)+v(129)*N(16)*N(15)+v(199)*N(25)*N(15)*N(27)+v(200)*N(25)*N(15)*N(1)+v(201)*N(25)*N(41)+v(203)*N(25)*N(25)-v(23)*N(50)*N(17)+v(24)*N(48)*N(18)+v(24)*N(48)*N(18)-v(25)*N(48)*N(17)*N(43)+v(26)*N(49)*N(18)+v(26)*N(49)*N(18)+v(26)*N(49)*N(18)-v(27)*N(49)*N(17)+v(28)*N(23)*N(18)-v(29)*N(23)*N(17)+v(30)*N(47)*N(18)-v(31)*N(47)*N(17)*N(43)+v(32)*N(29)*N(18)+v(32)*N(29)*N(18)-v(33)*N(29)*N(17)+v(34)*N(23)*N(1)+v(35)*N(24)*N(18)-v(36)*N(24)*N(17)*N(43)+v(37)*N(30)*N(18)+v(37)*N(30)*N(18)-v(38)*N(30)*N(17)*N(43)+v(39)*N(45)*N(18)+v(39)*N(45)*N(18)+v(39)*N(45)*N(18)-v(40)*N(45)*N(17)*N(43)+v(41)*N(46)*N(18)+v(41)*N(46)*N(18)+v(41)*N(46)*N(18)+v(41)*N(46)*N(18)-v(45)*N(26)*N(17)*N(27)-v(46)*N(26)*N(17)*N(1)+v(53)*N(44)*N(18)-v(54)*N(44)*N(17)-v(55)*N(44)*N(17)-v(94)*N(20)*N(17)*N(43) ;...    % H_2O
        +v(1)*N(51)*N(27)-v(100)*N(37)*N(18)-v(103)*N(38)*N(18)-v(113)*N(40)*N(18)*N(18)-v(114)*N(40)*N(18)*N(43)-v(115)*N(28)*N(18)+v(127)*N(11)*N(6)+v(129)*N(16)*N(15)+v(130)*N(16)*N(6)+v(131)*N(32)*N(33)+v(134)*N(32)*N(7)+v(137)*N(32)*N(1)+v(139)*N(32)*N(6)+v(140)*N(2)*N(27)+v(141)*N(2)*N(33)+v(144)*N(2)*N(19)+v(145)*N(2)*N(1)+v(149)*N(2)*N(6)+v(15)*N(18)*N(1)+v(157)*N(4)*N(6)+v(16)*N(18)*N(7)-v(164)*N(18)*N(27)*N(1)-v(165)*N(18)*N(1)*N(3)-v(166)*N(18)*N(1)*N(6)-v(167)*N(18)*N(1)*N(6)-v(168)*N(18)*N(3)-v(169)*N(18)*N(3)+v(17)*N(18)*N(27)-v(170)*N(18)*N(1)*N(1)-v(19)*N(18)*N(1)+v(2)*N(51)*N(1)+v(21)*N(32)*N(27)-v(22)*N(50)*N(18)-v(24)*N(48)*N(18)-v(26)*N(49)*N(18)-v(28)*N(23)*N(18)+v(3)*N(51)*N(27)-v(30)*N(47)*N(18)-v(32)*N(29)*N(18)-v(35)*N(24)*N(18)-v(37)*N(30)*N(18)-v(39)*N(45)*N(18)+v(4)*N(51)*N(1)-v(41)*N(46)*N(18)-v(42)*N(26)*N(18)*N(18)-v(43)*N(26)*N(18)*N(43)-v(44)*N(26)*N(18)-v(53)*N(44)*N(18)-v(57)*N(36)*N(18)-v(58)*N(31)*N(18)*N(18)-v(59)*N(31)*N(18)*N(43)-v(60)*N(31)*N(18)-v(69)*N(35)*N(18)-v(72)*N(34)*N(18)-v(78)*N(39)*N(18)*N(18)-v(79)*N(39)*N(18)*N(43)-v(91)*N(20)*N(18)*N(18)-v(92)*N(20)*N(18)*N(43)-v(93)*N(20)*N(18) ;...    % e
        -v(144)*N(2)*N(19)-v(204)*N(19)*N(27)-v(205)*N(19)*N(1)-v(206)*N(19)*N(6)-v(207)*N(19)*N(7)-v(208)*N(19)*N(3)-v(209)*N(19)*N(33)-v(210)*N(19)*N(3)-v(211)*N(19)*N(19)*N(1)-v(211)*N(19)*N(19)*N(1)-v(212)*N(19)+v(214)*N(53)*N(27)+v(215)*N(53)*N(1)+v(216)*N(53)*N(6)+v(217)*N(53)*N(7)+v(225)*N(54)*N(1)-v(227)*N(54)*N(19)-v(262)*N(60)*N(19)-v(263)*N(60)*N(19)+v(9)*N(18)*N(1) ;...    % O_2(a)
        +v(102)*N(37)*N(1)+v(104)*N(38)*N(1)+v(105)*N(40)*N(27)*N(43)+v(106)*N(40)*N(27)+v(107)*N(40)*N(7)+v(112)*N(40)*N(33)*N(43)+v(117)*N(28)*N(7)+v(16)*N(18)*N(7)+v(48)*N(26)*N(27)+v(49)*N(26)*N(21)+v(50)*N(26)*N(33)+v(51)*N(26)*N(7)+v(64)*N(31)*N(7)+v(67)*N(31)*N(6)+v(70)*N(35)*N(21)+v(74)*N(34)*N(7)+v(84)*N(39)*N(7)+v(85)*N(39)*N(6)*N(43)+v(87)*N(39)*N(1)+v(89)*N(39)*N(3)-v(90)*N(20)*N(9)*N(43)-v(91)*N(20)*N(18)*N(18)-v(92)*N(20)*N(18)*N(43)-v(93)*N(20)*N(18)-v(94)*N(20)*N(17)*N(43)-v(95)*N(20)*N(11)-v(96)*N(20)*N(8)-v(97)*N(20)*N(1)*N(27)-v(98)*N(20)*N(1)*N(1)-v(99)*N(20)*N(27)*N(27) ;...    % NO^+
        -v(124)*N(11)*N(21)+v(125)*N(11)*N(22)+v(128)*N(8)*N(7)-v(132)*N(32)*N(21)+v(134)*N(32)*N(7)+v(141)*N(2)*N(33)-v(142)*N(2)*N(21)-v(151)*N(4)*N(21)-v(152)*N(4)*N(21)-v(176)*N(33)*N(21)-v(177)*N(33)*N(21)-v(178)*N(33)*N(21)+v(183)*N(7)*N(22)+v(183)*N(7)*N(22)+v(184)*N(7)*N(3)-v(186)*N(21)*N(3)+v(187)*N(22)*N(22)+v(187)*N(22)*N(22)-v(192)*N(6)*N(21)+v(193)*N(6)*N(22)-v(49)*N(26)*N(21)-v(70)*N(35)*N(21)+v(95)*N(20)*N(11) ;...    % NO_2
        -v(125)*N(11)*N(22)+v(127)*N(11)*N(6)-v(143)*N(2)*N(22)-v(153)*N(4)*N(22)-v(183)*N(7)*N(22)-v(185)*N(21)*N(22)+v(186)*N(21)*N(3)-v(187)*N(22)*N(22)-v(187)*N(22)*N(22)-v(193)*N(6)*N(22)+v(96)*N(20)*N(8) ;...    % NO_3
        -v(28)*N(23)*N(18)-v(29)*N(23)*N(17)-v(34)*N(23)*N(1) ;...    % H_2O^+
        +v(29)*N(23)*N(17)-v(35)*N(24)*N(18)-v(36)*N(24)*N(17)*N(43)+v(55)*N(44)*N(17) ;...    % H_3O^+
        +v(172)*N(15)*N(41)+v(172)*N(15)*N(41)+v(173)*N(15)*N(3)+v(190)*N(6)*N(41)-v(196)*N(6)*N(25)*N(43)-v(197)*N(6)*N(25)-v(199)*N(25)*N(15)*N(27)-v(200)*N(25)*N(15)*N(1)-v(201)*N(25)*N(41)-v(202)*N(25)*N(3)-v(203)*N(25)*N(25)-v(203)*N(25)*N(25)+v(27)*N(49)*N(17)+v(29)*N(23)*N(17)+v(33)*N(29)*N(17)+v(55)*N(44)*N(17) ;...    % OH
        +v(108)*N(40)*N(7)+v(109)*N(40)*N(1)+v(110)*N(40)*N(3)+v(111)*N(40)*N(6)*N(43)+v(118)*N(28)*N(6)+v(15)*N(18)*N(1)+v(2)*N(51)*N(1)+v(34)*N(23)*N(1)-v(42)*N(26)*N(18)*N(18)-v(43)*N(26)*N(18)*N(43)-v(44)*N(26)*N(18)-v(45)*N(26)*N(17)*N(27)-v(46)*N(26)*N(17)*N(1)-v(47)*N(26)*N(27)*N(27)-v(48)*N(26)*N(27)-v(49)*N(26)*N(21)-v(50)*N(26)*N(33)-v(51)*N(26)*N(7)-v(52)*N(26)*N(1)*N(1)+v(65)*N(31)*N(1)+v(66)*N(31)*N(3)+v(71)*N(35)*N(1)+v(75)*N(34)*N(1)+v(86)*N(39)*N(1) ;...    % O_2^+
        -v(1)*N(51)*N(27)+v(100)*N(37)*N(18)+v(101)*N(37)*N(17)+v(102)*N(37)*N(1)-v(105)*N(40)*N(27)*N(43)-v(106)*N(40)*N(27)-v(11)*N(18)*N(27)-v(12)*N(18)*N(27)-v(17)*N(18)*N(27)+v(175)*N(33)*N(33)*N(43)+v(176)*N(33)*N(21)+v(177)*N(33)*N(21)+v(179)*N(33)*N(7)-v(21)*N(32)*N(27)+v(221)*N(54)*N(1)+v(222)*N(54)*N(52)+v(223)*N(54)*N(1)+v(224)*N(54)*N(27)+v(225)*N(54)*N(1)+v(226)*N(54)*N(1)+v(228)*N(54)*N(54)+v(229)*N(54)*N(54)+v(230)*N(54)*N(6)+v(231)*N(54)*N(7)+v(235)*N(55)*N(1)+v(237)*N(56)*N(1)+v(238)*N(58)*N(52)+v(241)*N(58)*N(7)+v(245)*N(59)*N(52)-v(3)*N(51)*N(27)-v(47)*N(26)*N(27)*N(27)-v(48)*N(26)*N(27)-v(5)*N(18)*N(27)+v(56)*N(36)*N(1)+v(57)*N(36)*N(18)+v(58)*N(31)*N(18)*N(18)+v(59)*N(31)*N(18)*N(43)-v(6)*N(18)*N(27)-v(61)*N(31)*N(27)*N(27)+v(63)*N(31)*N(33)+v(64)*N(31)*N(7)+v(65)*N(31)*N(1)+v(66)*N(31)*N(3)+v(68)*N(31)*N(6)+v(69)*N(35)*N(18)-v(7)*N(18)*N(27)+v(70)*N(35)*N(21)+v(71)*N(35)*N(1)+v(72)*N(34)*N(18)+v(72)*N(34)*N(18)+v(73)*N(34)*N(33)+v(73)*N(34)*N(33)+v(74)*N(34)*N(7)+v(74)*N(34)*N(7)+v(75)*N(34)*N(1)+v(75)*N(34)*N(1)+v(76)*N(34)*N(6)+v(76)*N(34)*N(6)-v(8)*N(18)*N(27)-v(80)*N(39)*N(27)*N(27)+v(82)*N(39)*N(7)-v(99)*N(20)*N(27)*N(27) ;...    % N_2
        -v(115)*N(28)*N(18)-v(116)*N(28)*N(17)-v(117)*N(28)*N(7)-v(118)*N(28)*N(6)+v(52)*N(26)*N(1)*N(1)+v(56)*N(36)*N(1) ;...    % O_4^+
        -v(32)*N(29)*N(18)-v(33)*N(29)*N(17)+v(54)*N(44)*N(17) ;...    % H_3O^+(OH)
        +v(33)*N(29)*N(17)+v(36)*N(24)*N(17)*N(43)-v(37)*N(30)*N(18)-v(38)*N(30)*N(17)*N(43) ;...    % H_5O_2^+
        +v(1)*N(51)*N(27)+v(17)*N(18)*N(27)-v(58)*N(31)*N(18)*N(18)-v(59)*N(31)*N(18)*N(43)-v(60)*N(31)*N(18)-v(61)*N(31)*N(27)*N(27)-v(62)*N(31)*N(33)*N(27)-v(63)*N(31)*N(33)-v(64)*N(31)*N(7)-v(65)*N(31)*N(1)-v(66)*N(31)*N(3)-v(67)*N(31)*N(6)-v(68)*N(31)*N(6)+v(81)*N(39)*N(33)*N(43)+v(83)*N(39)*N(7) ;...    % N_2^+
        -v(131)*N(32)*N(33)-v(132)*N(32)*N(21)-v(133)*N(32)*N(7)*N(43)-v(134)*N(32)*N(7)-v(135)*N(32)*N(1)*N(43)-v(136)*N(32)*N(1)-v(137)*N(32)*N(1)-v(138)*N(32)*N(3)-v(139)*N(32)*N(6)+v(148)*N(2)*N(6)+v(163)*N(13)*N(6)+v(167)*N(18)*N(1)*N(6)+v(168)*N(18)*N(3)+v(19)*N(18)*N(1)-v(21)*N(32)*N(27) ;...    % O^-
        +v(105)*N(40)*N(27)*N(43)+v(106)*N(40)*N(27)+v(108)*N(40)*N(7)+v(11)*N(18)*N(27)-v(112)*N(40)*N(33)*N(43)+v(12)*N(18)*N(27)+v(12)*N(18)*N(27)-v(131)*N(32)*N(33)-v(141)*N(2)*N(33)-v(175)*N(33)*N(33)*N(43)-v(175)*N(33)*N(33)*N(43)-v(176)*N(33)*N(21)-v(177)*N(33)*N(21)-v(178)*N(33)*N(21)-v(179)*N(33)*N(7)-v(180)*N(33)*N(3)-v(181)*N(33)*N(6)*N(43)-v(182)*N(33)*N(1)-v(209)*N(19)*N(33)+v(222)*N(54)*N(52)+v(242)*N(58)*N(27)+v(243)*N(58)*N(6)+v(251)*N(59)*N(7)+v(3)*N(51)*N(27)-v(50)*N(26)*N(33)+v(60)*N(31)*N(18)+v(60)*N(31)*N(18)-v(62)*N(31)*N(33)*N(27)-v(63)*N(31)*N(33)+v(67)*N(31)*N(6)+v(69)*N(35)*N(18)+v(71)*N(35)*N(1)-v(73)*N(34)*N(33)+v(77)*N(39)*N(6)+v(78)*N(39)*N(18)*N(18)+v(79)*N(39)*N(18)*N(43)-v(81)*N(39)*N(33)*N(43)+v(84)*N(39)*N(7)+v(86)*N(39)*N(1)+v(93)*N(20)*N(18) ;...    % N
        +v(61)*N(31)*N(27)*N(27)-v(72)*N(34)*N(18)-v(73)*N(34)*N(33)-v(74)*N(34)*N(7)-v(75)*N(34)*N(1)-v(76)*N(34)*N(6) ;...    % N_4^+
        +v(62)*N(31)*N(33)*N(27)-v(69)*N(35)*N(18)-v(70)*N(35)*N(21)-v(71)*N(35)*N(1)+v(80)*N(39)*N(27)*N(27) ;...    % N_3^+
        +v(47)*N(26)*N(27)*N(27)-v(56)*N(36)*N(1)-v(57)*N(36)*N(18) ;...    % O_2^+N_2
        -v(100)*N(37)*N(18)-v(101)*N(37)*N(17)-v(102)*N(37)*N(1)+v(99)*N(20)*N(27)*N(27) ;...    % NO^+N_2
        -v(103)*N(38)*N(18)-v(104)*N(38)*N(1)+v(97)*N(20)*N(1)*N(27)+v(98)*N(20)*N(1)*N(1) ;...    % NO^+O_2
        +v(3)*N(51)*N(27)+v(63)*N(31)*N(33)+v(73)*N(34)*N(33)-v(77)*N(39)*N(6)-v(78)*N(39)*N(18)*N(18)-v(79)*N(39)*N(18)*N(43)-v(80)*N(39)*N(27)*N(27)-v(81)*N(39)*N(33)*N(43)-v(82)*N(39)*N(7)-v(83)*N(39)*N(7)-v(84)*N(39)*N(7)-v(85)*N(39)*N(6)*N(43)-v(86)*N(39)*N(1)-v(87)*N(39)*N(1)-v(88)*N(39)*N(1)-v(89)*N(39)*N(3) ;...    % N^+
        -v(105)*N(40)*N(27)*N(43)-v(106)*N(40)*N(27)-v(107)*N(40)*N(7)-v(108)*N(40)*N(7)-v(109)*N(40)*N(1)-v(110)*N(40)*N(3)-v(111)*N(40)*N(6)*N(43)-v(112)*N(40)*N(33)*N(43)-v(113)*N(40)*N(18)*N(18)-v(114)*N(40)*N(18)*N(43)+v(4)*N(51)*N(1)+v(68)*N(31)*N(6)+v(76)*N(34)*N(6)+v(77)*N(39)*N(6)+v(82)*N(39)*N(7)+v(88)*N(39)*N(1) ;...    % O^+
        +v(130)*N(16)*N(6)-v(171)*N(15)*N(41)-v(172)*N(15)*N(41)-v(174)*N(41)*N(41)-v(174)*N(41)*N(41)+v(188)*N(1)*N(15)*N(43)+v(189)*N(1)*N(15)*N(43)-v(190)*N(6)*N(41)+v(196)*N(6)*N(25)*N(43)-v(201)*N(25)*N(41)+v(202)*N(25)*N(3) ;...    % HO_2
        +v(171)*N(15)*N(41) ;...    % H_2
        0 ;...    % M
        +v(116)*N(28)*N(17)+v(45)*N(26)*N(17)*N(27)+v(46)*N(26)*N(17)*N(1)-v(53)*N(44)*N(18)-v(54)*N(44)*N(17)-v(55)*N(44)*N(17) ;...    % O_2^+(H_2O)
        +v(27)*N(49)*N(17)+v(38)*N(30)*N(17)*N(43)-v(39)*N(45)*N(18)-v(40)*N(45)*N(17)*N(43) ;...    % H_7O_3^+
        +v(40)*N(45)*N(17)*N(43)-v(41)*N(46)*N(18) ;...    % H_9O_4^+
        +v(101)*N(37)*N(17)+v(23)*N(50)*N(17)-v(30)*N(47)*N(18)-v(31)*N(47)*N(17)*N(43)+v(94)*N(20)*N(17)*N(43) ;...    % (H_2O)NO^+
        -v(24)*N(48)*N(18)-v(25)*N(48)*N(17)*N(43)+v(31)*N(47)*N(17)*N(43) ;...    % (H_2O)_2NO^+
        +v(25)*N(48)*N(17)*N(43)-v(26)*N(49)*N(18)-v(27)*N(49)*N(17) ;...    % (H_2O)_3NO^+
        -v(22)*N(50)*N(18)-v(23)*N(50)*N(17)+v(90)*N(20)*N(9)*N(43) ;...    % CO_2NO^+
        0 ;...    % Q
        +v(21)*N(32)*N(27)+v(220)*N(54)*N(1)-v(222)*N(54)*N(52)-v(238)*N(58)*N(52)-v(244)*N(59)*N(52)-v(245)*N(59)*N(52) ;...    % N_2O
        +v(10)*N(18)*N(1)-v(213)*N(53)-v(214)*N(53)*N(27)-v(215)*N(53)*N(1)-v(216)*N(53)*N(6)-v(217)*N(53)*N(7)-v(218)*N(53)*N(3)+v(226)*N(54)*N(1)+v(246)*N(59)*N(1)+v(262)*N(60)*N(19) ;...    % O_2(b)
        -v(219)*N(54)*N(6)-v(220)*N(54)*N(1)-v(221)*N(54)*N(1)-v(222)*N(54)*N(52)-v(223)*N(54)*N(1)-v(224)*N(54)*N(27)-v(225)*N(54)*N(1)-v(226)*N(54)*N(1)-v(227)*N(54)*N(19)-v(228)*N(54)*N(54)-v(228)*N(54)*N(54)-v(229)*N(54)*N(54)-v(229)*N(54)*N(54)-v(230)*N(54)*N(6)-v(231)*N(54)*N(7)+v(232)*N(55)*N(27)+v(233)*N(55)+v(234)*N(55)*N(7)+v(5)*N(18)*N(27) ;...    % N_2(A)
        +v(227)*N(54)*N(19)+v(228)*N(54)*N(54)-v(232)*N(55)*N(27)-v(233)*N(55)-v(234)*N(55)*N(7)-v(235)*N(55)*N(1)+v(236)*N(56)+v(6)*N(18)*N(27) ;...    % N_2(B)
        +v(229)*N(54)*N(54)-v(236)*N(56)-v(237)*N(56)*N(1)+v(8)*N(18)*N(27) ;...    % N_2(C)
        +v(7)*N(18)*N(27) ;...    % N_2(a)
        +v(11)*N(18)*N(27)+v(219)*N(54)*N(6)-v(238)*N(58)*N(52)-v(239)*N(58)*N(1)-v(240)*N(58)*N(1)-v(241)*N(58)*N(7)-v(242)*N(58)*N(27)-v(243)*N(58)*N(6) ;...    % N(^2D)
        +v(13)*N(18)*N(1)+v(210)*N(19)*N(3)+v(240)*N(58)*N(1)+v(243)*N(58)*N(6)-v(244)*N(59)*N(52)-v(245)*N(59)*N(52)-v(246)*N(59)*N(1)-v(247)*N(59)*N(27)-v(248)*N(59)*N(1)-v(249)*N(59)*N(3)-v(250)*N(59)*N(3)-v(251)*N(59)*N(7)-v(252)*N(59)+v(253)*N(60)+v(255)*N(60)*N(1)+v(256)*N(60)*N(3)+v(259)*N(60)*N(7)+v(261)*N(60)*N(52)+v(262)*N(60)*N(19)+v(264)*N(60)*N(6) ;...    % O(^1D)
        +v(14)*N(18)*N(1)+v(230)*N(54)*N(6)+v(237)*N(56)*N(1)-v(253)*N(60)-v(254)*N(60)*N(27)-v(255)*N(60)*N(1)-v(256)*N(60)*N(3)-v(257)*N(60)*N(3)-v(258)*N(60)*N(7)-v(259)*N(60)*N(7)-v(260)*N(60)*N(52)-v(261)*N(60)*N(52)-v(262)*N(60)*N(19)-v(263)*N(60)*N(19)-v(264)*N(60)*N(6) ;...    % O(^1S)
        +v(213)*N(53) ;...    % hy(O_2Atm)
        +v(212)*N(19) ;...    % hy(O_2IRAtm)
        +v(253)*N(60) ;...    % hy(557nm)
        +v(252)*N(59) ;...    % hy(630nm)
        +v(236)*N(56) ;...    % hy(2PN_2)
        +v(233)*N(55) ;...    % hy(1PN_2)
        +v(174)*N(41)*N(41) ;...    % H_2O_2
        0 ;...    % hyPhCh00
        0 ;...    % hyPhCh01
        0 ;...    % hyPhCh02
        0 ;...    % hyPhCh03
        0 ;...    % hyPhCh04
        0 ;...    % hyPhCh05
        0 ;...    % hyPhCh06
        0 ;...    % hyPhCh07
        0 ;...    % hyPhCh08
        0 ;...    % hyPhCh09
        0 ;...    % hyPhCh10
        0 ;...    % hyPhCh11
        0 ;...    % hyPhCh12
        0 ;...    % hyPhCh13
        0 ;...    % hyPhCh14
        0 ;...    % hyPhCh15
        0 ;...    % hyPhCh16
        0 ;...    % hyPhCh17
        0 ;...    % hyPhCh18
        0 ;...    % hyPhCh19
        0 ;...    % hyPhCh20
        0 ;...    % hyPhCh21
        0 ;...    % hyPhCh22
        0 ;...    % hyPhCh23
        0 ;...    % hyPhCh24
        0 ;...    % hyPhCh25
        0 ;...    % hyPhCh26
        0 ;...    % hyPhCh27
        0 ;...    % hyPhCh28
    ];
end

% autogenerated by reactor revision 'b4615705f07c'
