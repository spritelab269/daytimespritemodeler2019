% This file is a part of Daytime Sprite Modeler
% Copyright © 2019 IAP RAS, see LICENSE.txt file for EULA

function e_index = get_index_e
    e_index = [18];
end

% autogenerated by reactor revision 'b4615705f07c'
