% This file is a part of Daytime Sprite Modeler
% Copyright © 2019 IAP RAS, see LICENSE.txt file for EULA

%{
DOC
Величина дополнительного источника ионизации зависит от высоты
$$
    I_{GCR} = 7\cdot 10^{-20} e^{(z-65000)/3000}
$$
%}

function I = get_ionization(zz)
    % I = get_ionization(zz)
    % returns GCR-ionization depens on altitude zz
    I = 7e-20*exp((zz-65000)./3000);
end
