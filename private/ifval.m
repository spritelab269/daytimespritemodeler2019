% This file is a part of Daytime Sprite Modeler
% Copyright © 2019 IAP RAS, see LICENSE.txt file for EULA

function r=ifval(c,a,b)
	if c
		r=a;
	else
		r=b;
	end	
end
