% This file is a part of Daytime Sprite Modeler
% Copyright © 2019 IAP RAS, see LICENSE.txt file for EULA

% log10space Logarithmically spaced vector.
% log10space(X1, X2) generates a row vector of 100 logarithmically
%    equally spaced points between X1 and X2.
% 
%    log10space(X1, X2, N) generates N points between X1 and X2.
%    For N = 1, log10space returns X2.

function v = log10space(a,b,n)
    if nargin==2
        n = 100;
    end
    L=b-a;
    m = 1e10/L;
    a1 = log10(1);
    b1 = log10(L*m+1);
    p = linspace(a1,b1,n);
    v = ((10.^p)-1+a)./m;
end