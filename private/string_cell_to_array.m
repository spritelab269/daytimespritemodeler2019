% This file is a part of Daytime Sprite Modeler
% Copyright © 2019 IAP RAS, see LICENSE.txt file for EULA

function array = string_cell_to_array(string_cell)
    % array = string_cell_to_array(string_cell)
    % string cell is cell of strings
    % array is 2D matrix of chars
    
    lens = cellfun(@length, string_cell);
    maxlen = max(lens);
    array = zeros(numel(string_cell), maxlen, 'int8');
    for i=1:numel(string_cell)
        array(i,1:length(string_cell{i})) = string_cell{i};
    end
    array = char(array);
end
