% This file is a part of Daytime Sprite Modeler
% Copyright © 2019 IAP RAS, see LICENSE.txt file for EULA

function r=ifval(x, x0, a, y1, y2)
    t = (tanh((x-x0)*a)+1)/2;
    r = (1-t).*y1 + t .* y2;
end
