% This file is a part of Daytime Sprite Modeler
% Copyright © 2019 IAP RAS, see LICENSE.txt file for EULA

function sv = vector_to_string_cell(v)
    sv = cell(size(v));
    for i=1:numel(v)
        sv{i} = num2str(v(i));
    end
end