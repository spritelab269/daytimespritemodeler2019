% This file is a part of Daytime Sprite Modeler
% Copyright © 2019 IAP RAS, see LICENSE.txt file for EULA

% function value = rN
% rN constant

function value = rN
	value = 2;
end

% autogenerated by reactor revision 'b4615705f07c'
