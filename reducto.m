% This file is a part of Daytime Sprite Modeler
% Copyright © 2019 IAP RAS, see LICENSE.txt file for EULA

function r = reducto(v)
    % r = reducto(v)
    % reshame multidimensional matrix reducing dimensions with size 1
    s = size(v);
    r = reshape(v, s(s>1));
end

