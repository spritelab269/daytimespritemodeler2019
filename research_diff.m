% This file is a part of Daytime Sprite Modeler
% Copyright © 2019 IAP RAS, see LICENSE.txt file for EULA

function [rates0, rates1, b_ineq, N0, dt, zzr] = research_diff
    S = load_sprite_data(filename);

    tt = S.t1;
    NN = S.N1;

    ti = 1;

    get_z_index = @(z) interp1(S.z, 1:numel(S.z), z, 'nearest');

    zzr = 40000:5000:70000;
    
    rates0 = zeros(size(S.rates1, 2), numel(zzr));
    rates1 = zeros(size(S.rates1, 2), numel(zzr));
    b_ineq = zeros(size(S.N1, 2), numel(zzr));
    N0 = zeros(size(S.N1, 2), numel(zzr));
    
    ri = 1;
    dt = tt(ti+1) - tt(ti);
    
    zri = 1;
    for zr = zzr
        zi = get_z_index(zr);


        N = NN(ti, :, zi, ri);
        N0(:, zri) = N;

        dN0 = diffN(tt(ti), N, zi, ri);
        for spi=1:numel(S.titles)
            fprintf('%20s\t%1.6e\t%1.6e\n', S.titles{spi}, N(spi), dN0(spi));
        end

        [T, TE, q, Mn] = getVParamsRelax(tt(ti), N, zi, ri);
        rates0(:, zri) = get_rates(T, TE, q, Mn);    % 290 x 1

        Aeq = rates_system(N);   % 97x290
        size(Aeq)
        beq = zeros(size(N'));    % 97 x 1
        size(beq)

        alpha = 0.1;
        H = 2*(Aeq'*Aeq)+2*alpha*eye(numel(rates0(:, zri)));
        f = -2*rates0(:, zri)';

        rates1(:, zri) = quadprog(H, f, [], [], [], [], rates0(:, zri)*0.9, rates0(:, zri)/0.9);

        b_ineq(:, zri) = Aeq * rates1(:, zri);

        % results = [rates1, rates0, rates1 - rates0, (rates1 - rates0)./rates0];
        zri = zri + 1;
    end
    
end


% copy-pasted from Sprite.m

function dN = diffN(t, N, zi, ri)
    [T, TE, q, Mn] = getVParamsRelax(t, N, zi, ri);
    v = get_rates(T, TE, q, Mn);
    dN = [...
        diff_common(N,v);
        getDE0(t, N, ri, zi);...     % E
    ];
end


%{
DOC
Динамика концентраций во время спайта описывается системой дифференциальных уравнений
\begin{align}
\frac{dN_i}{dt} =& \sum\limits_j \widetilde v_j(T, T_e, \theta, N_m)\prod\limits_k N_{j_k} \\
              i =& 1, \dots, n_{\mathrm{species}} \\
  \frac{dE}{dt} =&  -\frac{\sigma E}{\varepsilon_0} + \frac{dE_{\mathrm{ext}}}{dt} p_e
\end{align}
%}

function dN = diffNE(t, N, zi, ri)
    if any(isinf(N))
        error('diffNE:N_inf', 'incorrect values');
    end
    if any(isnan(N))
        error('diffNE:N_nan', 'incorrect values');
    end
    [T, TE, q, Mn] = getVParamsRelax(t, N, zi, ri);
    v = get_rates(T, TE, q, Mn);
    dN = [...
        diff_common(N,v);
        getDE(t, N, ri, zi);...     % E
    ];
end


%{
DOC
\paragraph{Электрическое поле на стадии релаксации}
$$
    \frac{dE}{dt} = -\frac{\sigma E}{\varepsilon_0}
$$
%}

function dE = getDE0(tt, N, ri, zi)
    dE = -getSigma(tt, N)*N(get_index_ef)/eps_0;
end


%{
DOC
\paragraph{Электрическое поле во время спрайта}
$$
    \frac{dE}{dt} = -\frac{\sigma E}{\varepsilon_0} + \frac{dE_{\mathrm{ext}}}{dt} p_e
$$
%}

function dE = getDE(tt, N, ri, zi)
    global electric_factor
    E = N(get_index_ef);
    Qd = getQd(tt);
    if numel(electric_factor) == 0
        electric_factor = calculate_electric_factor;
    end
    ef = electric_factor(zi, ri);
    dE = -getSigma(tt,N)*E/eps_0 + getQd(tt)*ef;
    if isnan(E)
        error('getDE:E_nan', ...
              't = %g\tsigma = %g\tE = %g\tQd = %g\tef = %g\n', ...
              tt, getSigma(tt,N), E, Qd, ef);
    end
end


function [T, TE, q, Mn] = getVParamsRelax(t, N, zi, ri)
    Mn = N(get_index_neutral);
    T = getTByZ(zz(zi));
    E = N(get_index_ef);
    q = max(q0, 1.0 / 100.0 * E / Mn * 1e17);
    TE = T;
end


function [T, TE, q, Mn] = getVParamsSprite(t, N, zi, ri)
    z = zz(zi);
    % Все нормально, N здесь уже при конкретных z, r, t
    Mn = N(get_index_neutral);
    T  = getTByZ(z);
    q  = q0;

    % E должно быть в В/м, theta будет в Td, *10 - чтобы было в Td,
    % 0.01 чтобы перевести В/м в В/см
    % Каждые 10^-17 В см2 - это 1 Td
    E = N(get_index_ef);
    q = max(q0, 1.0 / 100.0 * E / Mn * 1e17);
    % q = 1.0 / 100.0 * E / Mn * 1e17;
    TE = max(T, getTe(q));
end


function sigma_e = getSigma(t, S)
    if size(S,1)>1 && size(S,2)>1 && size(S,3)>1 && size(S,4)>1
        N_e = S(:,get_index_e,:,:);
        N_m = S(:,get_index_neutral,:,:);
        E = S(:,get_index_ef,:,:);
    elseif size(S,1)>1 && size(S,2)>1 && size(S,3)>1
        N_e = S(:,get_index_e,:);
        N_m = S(:,get_index_neutral,:);
        E = S(:,get_index_ef,:);
    elseif size(S,1)>1 && size(S,2)>1
        N_e = S(:,get_index_e);
        N_m = S(:,get_index_neutral);
        E = S(:,get_index_ef);
    else
        N_e = S(get_index_e);
        N_m = S(get_index_neutral);
        E = S(get_index_ef);
    end

    sigma_e = getSigmaByNeNmTe(N_e, N_m, getTe(E./N_m.*10.*1e14));
end


%{
DOC
Проводимость в СИ зависит от концентрации электронов, нейтралов и
температуры электронов так
$$
    \sigma_e = \frac{q_e^2 \cdot 10^7 \cdot N_e}{10^{-3}m_e \nu_e(N_M,
    T_e)}.
$$
$q_e = 1.6\cdot 10^{-19}$ Кл --- заряд электрона, $m_e = 9.1 \cdot 10^{-28}$ г --- масса электрона.
%}

function sigma_e = getSigmaByNeNmTe(N_e, N_m, Te)
    e_val = 1.6e-19;    % Culon
    m_e = 9.1e-28;      % electron mass in gramms
    %e_val = 4.80320427e-10; % is SGS

    % in SGS
    %sigma_e = 9e9.*(e_val^2.*N_e)./(m_e.*getNuE(N_m,getTe(E,getEp(N_m))));

    % in SI
    sigma_e = (e_val^2.*1000000.*N_e)./(1e-3*m_e.*getNuE(N_m,Te));
end


function v = getI(t)
    % v = getI(t)
    % I(t) - ток в момент времени t
    ttau1sqr = (t./tau1).^2;
    ttau2 = t./tau2;
    v = I0.* (ttau1sqr.*exp(-ttau2))./(1+ttau1sqr);
end

%{
DOC
\paragraph{Частота столкновений электронов} зависит от концентрации нейтралов и температуры электронов по некоторой\footnote{?} формуле
\begin{equation}\label{nu_e}
    \nu_e = 1.84\times 10^9 \left( \frac{N_m}{10^{17} \mbox{см}^{-3}} \right) \left( \frac{T_e [K]}{ 1000K} \right)^{5/6} \mbox{с}^{-1}.
\end{equation}
Вообще, это работает для больших энергий, больше 10000K, но нам кажется, что допустимо использовать эту формулу и для низких энергий (порядка 200K).
Контрольные значения из работы Мареева, Яшунина, 2010.
$$
    \nu_e = 1.1\times 10^6 \mbox{с}^{-1} @ 80 km,
$$
$$
    \nu_e = 1.6\times 10^5 \mbox{с}^{-1} @ 90 km.
$$
%}

function nuE = getNuE(Nm, Te)
    nuE = 1.84e9 * (Nm./1e17) .* ((Te/1000).^(5/6));
end

%{
DOC
TODO: \texttt{getQ}
%}

function v = getQ_piece(t)
    % v = getQ(t)
    % q(t) - перенесенный заряд к моменту времен t
    if t <= t0sprite
        v = integral(@getI, 0, t);
    elseif t < t1sprite
        v = getQ(t0sprite).*(1-(t-t0sprite)./(t1sprite-t0sprite));
    else
        v = 0;
    end
end

function v = getQ(t)
    if t < t1sprite
        v = integral(@getQd, 0, t);
    else
        v = 0;
    end
end

%{
DOC
TODO: \texttt{getQd}
%}

function v = getQd_piece(t)
    % v = getQd(t)
    % q'(t) - производная перенесенного заряда в момент времени t
    if t < t0sprite
        v = getI(t);
    elseif t == t1sprite
        v = 0;
    elseif t < t1sprite
        v = -getQ(t0sprite)/(t1sprite-t0sprite);
    else
        v = 0;
    end
end

function v = getQd(t)
    v = zeros(size(t));
    step_fun = (tanh((t(t<=t1sprite)-t0sprite)*step_param)+1)/2;
    v(t<=t1sprite) = getI(t(t<=t1sprite)) .* (1-step_fun) - integral(@getI, 0, t0sprite) / (t1sprite-t0sprite).* step_fun;
end


%{
DOC
Температура нейтралов $T$ взята из модели WACCM. А может NRLMSISE.
Считается линейной интерполяцией.
%}

function T = getTByZ(z)
    zzs = 0:1e3:150e3;
    tts = [287.668;        283.915;        278.286;        271.358;        263.672;
        255.694;        247.809;        240.319;        233.454;        227.393;
        222.276;        218.178;        214.999;        212.626;        210.966;
        209.948;        209.503;        209.533;        209.938;        210.622;        211.487;
        212.453;        213.509;        214.659;        215.909;        217.267;        218.738;        220.331;
        222.053;        223.913;        225.921;        228.087;        230.422;        232.958;        235.79;
        238.883;        242.176;        245.602;        249.086;        252.543;        255.881;        258.996;
        261.779;        264.112;        265.879;        266.959;        267.272;        266.858;        265.798;
        264.18;        262.095;        259.637;        256.895;        253.956;        250.899;        247.8;
        244.718;        241.683;        238.713;        235.827;        233.04;        230.366;        227.817;
        225.406;        223.141;        221.034;        219.092;        217.326;        215.742;        214.35;
        213.158;        212.175;        211.411;        210.865;        210.451;        210.123;        209.842;
        209.57;        209.272;        208.911;        208.451;        207.858;        207.099;        206.143;        204.959;
        203.522;        201.809;        199.801;        197.486;        194.854;        191.903;        188.669;        185.317;
        182.025;        178.946;        176.213;        173.944;        172.246;        171.229;        171.006;        171.712;
        173.469;        176.26;        180.055;        184.842;        190.622;        197.404;        205.198;        214.007;
        223.814;        234.573;        246.219;        258.762;        272.227;        286.623;        301.931;
        318.098;        335.024;        352.551;        370.443;        388.381;        405.948;       422.628;
        437.817;        451.515;        464.581;        477.386;        489.935;        502.233;
        514.286;        526.098;        537.675;        549.02;        560.139;        571.037;        581.717;
        592.184;        602.443;        612.498;        622.352;       632.01;        641.476;        650.754;        659.847;
        668.76;        677.495;        686.057;        694.449;        702.674;        710.736;        718.638];
    T = interp1(zzs, tts, z, 'linear');
end


%{
DOC
Температура электронов $T_e$ зависит от электрического поля.
Когда-то мы вывели в программе Bolsig+ эту зависимость для некоторого
частного случая и теперь пользуемся ею.
%}

function Te = getTe(theta)
    if numel(theta)~=1
        Te = zeros(size(theta));
        for i=1:numel(theta)
            Te(i) = getTe(theta(i));
        end
    else
        if theta<0
            theta=0;
        end
        if theta>500
            error('theta>500');
        end

        Te = interp1([0 0.1000d-03, 2.513d0, 5.025d0, 7.538d0, 10.05d0, 12.56d0, 15.08d0, 17.59d0,...
            20.10d0, 22.61d0, 25.13d0, 27.64d0, 30.15d0, 32.66d0, 35.18d0, 37.69d0, 40.20d0, 42.71d0,...
            45.23d0, 47.74d0, 50.25d0, 52.76d0, 55.28d0, 57.79d0, 60.30d0, 62.81d0, 65.33d0, 67.84d0,...
            70.35d0, 72.86d0, 75.38d0, 77.89d0, 80.40d0, 82.91d0, 85.43d0, 87.94d0, 90.45d0, 92.96d0,...
            95.48d0, 97.99d0, 100.5d0, 103.0d0, 105.5d0, 108.0d0, 110.6d0, 113.1d0, 115.6d0, 118.1d0,...
            120.6d0, 123.1d0, 125.6d0, 128.1d0, 130.7d0, 133.2d0, 135.7d0, 138.2d0, 140.7d0, 143.2d0,...
            145.7d0, 148.2d0, 150.8d0, 153.3d0, 155.8d0, 158.3d0, 160.8d0, 163.3d0, 165.8d0, 168.3d0,...
            170.9d0, 173.4d0, 175.9d0, 178.4d0, 180.9d0, 183.4d0, 185.9d0, 188.4d0, 191.0d0, 193.5d0,...
            196.0d0, 198.5d0, 201.0d0, 203.5d0, 206.0d0, 208.5d0, 211.1d0, 213.6d0, 216.1d0, 218.6d0,...
            221.1d0, 223.6d0, 226.1d0, 228.6d0, 231.2d0, 233.7d0, 236.2d0, 238.7d0, 241.2d0, 243.7d0,...
            246.2d0, 248.7d0, 251.3d0, 253.8d0, 256.3d0, 258.8d0, 261.3d0, 263.8d0, 266.3d0, 268.8d0,...
            271.4d0, 273.9d0, 276.4d0, 278.9d0, 281.4d0, 283.9d0, 286.4d0, 288.9d0, 291.5d0, 294.0d0,...
            296.5d0, 299.0d0, 301.5d0, 304.0d0, 306.5d0, 309.0d0, 311.6d0, 314.1d0, 316.6d0, 319.1d0,...
            321.6d0, 324.1d0, 326.6d0, 329.1d0, 331.7d0, 334.2d0, 336.7d0, 339.2d0, 341.7d0, 344.2d0,...
            346.7d0, 349.2d0, 351.8d0, 354.3d0, 356.8d0, 359.3d0, 361.8d0, 364.3d0, 366.8d0, 369.3d0,...
            371.9d0, 374.4d0, 376.9d0, 379.4d0, 381.9d0, 384.4d0, 386.9d0, 389.4d0, 392.0d0, 394.5d0,...
            397.0d0, 399.5d0, 402.0d0, 404.5d0, 407.0d0, 409.5d0, 412.1d0, 414.6d0, 417.1d0, 419.6d0,...
            422.1d0, 424.6d0, 427.1d0, 429.6d0, 432.2d0, 434.7d0, 437.2d0, 439.7d0, 442.2d0, 444.7d0,...
            447.2d0, 449.7d0, 452.3d0, 454.8d0, 457.3d0, 459.8d0, 462.3d0, 464.8d0, 467.3d0, 469.8d0,...
            472.4d0, 474.9d0, 477.4d0, 479.9d0, 482.4d0, 484.9d0, 487.4d0, 489.9d0, 492.5d0, 495.0d0, 497.5d0, 500.0d0], ...
            [0.6112d-02,0.6112d-02, 0.6675d0, 0.8394d0, 0.9170d0, 0.9615d0, 0.9904d0, 1.011d0, 1.027d0,...
            1.040d0, 1.051d0, 1.060d0, 1.069d0, 1.077d0, 1.085d0, 1.094d0, 1.102d0, 1.112d0, 1.122d0,...
            1.134d0, 1.149d0, 1.165d0, 1.185d0, 1.208d0, 1.237d0, 1.274d0, 1.302d0, 1.342d0, 1.396d0,...
            1.432d0, 1.483d0, 1.532d0, 1.597d0, 1.667d0, 1.733d0, 1.796d0, 1.865d0, 1.947d0, 2.033d0,...
            2.112d0, 2.182d0, 2.269d0, 2.357d0, 2.442d0, 2.523d0, 2.605d0, 2.688d0, 2.773d0, 2.854d0,...
            2.939d0, 3.024d0, 3.110d0, 3.191d0, 3.263d0, 3.340d0, 3.415d0, 3.490d0, 3.576d0, 3.651d0,...
            3.726d0, 3.794d0, 3.858d0, 3.925d0, 3.993d0, 4.066d0, 4.132d0, 4.201d0, 4.270d0, 4.337d0,...
            4.382d0, 4.444d0, 4.503d0, 4.557d0, 4.611d0, 4.663d0, 4.737d0, 4.792d0, 4.845d0, 4.914d0,...
            4.970d0, 5.024d0, 5.077d0, 5.128d0, 5.169d0, 5.220d0, 5.244d0, 5.290d0, 5.336d0, 5.385d0,...
            5.435d0, 5.485d0, 5.534d0, 5.583d0, 5.626d0, 5.673d0, 5.739d0, 5.781d0, 5.822d0, 5.863d0,...
            5.904d0, 5.943d0, 5.983d0, 6.022d0, 6.061d0, 6.100d0, 6.141d0, 6.176d0, 6.227d0, 6.269d0,...
            6.312d0, 6.353d0, 6.395d0, 6.436d0, 6.476d0, 6.516d0, 6.555d0, 6.592d0, 6.629d0, 6.665d0,...
            6.702d0, 6.739d0, 6.776d0, 6.812d0, 6.851d0, 6.886d0, 6.922d0, 6.957d0, 6.992d0, 7.027d0,...
            7.062d0, 7.098d0, 7.136d0, 7.174d0, 7.211d0, 7.249d0, 7.286d0, 7.322d0, 7.359d0, 7.395d0,...
            7.431d0, 7.467d0, 7.502d0, 7.538d0, 7.573d0, 7.607d0, 7.642d0, 7.678d0, 7.714d0, 7.751d0,...
            7.785d0, 7.818d0, 7.851d0, 7.884d0, 7.918d0, 7.951d0, 7.984d0, 8.017d0, 8.050d0, 8.084d0,...
            8.118d0, 8.152d0, 8.186d0, 8.219d0, 8.253d0, 8.288d0, 8.322d0, 8.357d0, 8.391d0, 8.426d0,...
            8.460d0, 8.493d0, 8.527d0, 8.562d0, 8.596d0, 8.631d0, 8.665d0, 8.700d0, 8.734d0, 8.768d0,...
            8.804d0, 8.837d0, 8.870d0, 8.903d0, 8.936d0, 8.968d0, 9.001d0, 9.033d0, 9.066d0, 9.099d0,...
            9.131d0, 9.163d0, 9.195d0, 9.228d0, 9.260d0, 9.292d0, 9.325d0, 9.357d0, 9.390d0, 9.422d0,...
            9.454d0, 9.487d0]*11604, ...
            theta);
    end
end

function Te = getTeNew(theta)

        theta = max(zeros(size(theta)), theta);
        
        if numel(find(theta>500)) > 0
            error('theta>500');
        end
        Te = zeros(size(theta));
        
        Te(:) = interp1([0 0.1000d-03, 2.513d0, 5.025d0, 7.538d0, 10.05d0, 12.56d0, 15.08d0, 17.59d0,...
            20.10d0, 22.61d0, 25.13d0, 27.64d0, 30.15d0, 32.66d0, 35.18d0, 37.69d0, 40.20d0, 42.71d0,...
            45.23d0, 47.74d0, 50.25d0, 52.76d0, 55.28d0, 57.79d0, 60.30d0, 62.81d0, 65.33d0, 67.84d0,...
            70.35d0, 72.86d0, 75.38d0, 77.89d0, 80.40d0, 82.91d0, 85.43d0, 87.94d0, 90.45d0, 92.96d0,...
            95.48d0, 97.99d0, 100.5d0, 103.0d0, 105.5d0, 108.0d0, 110.6d0, 113.1d0, 115.6d0, 118.1d0,...
            120.6d0, 123.1d0, 125.6d0, 128.1d0, 130.7d0, 133.2d0, 135.7d0, 138.2d0, 140.7d0, 143.2d0,...
            145.7d0, 148.2d0, 150.8d0, 153.3d0, 155.8d0, 158.3d0, 160.8d0, 163.3d0, 165.8d0, 168.3d0,...
            170.9d0, 173.4d0, 175.9d0, 178.4d0, 180.9d0, 183.4d0, 185.9d0, 188.4d0, 191.0d0, 193.5d0,...
            196.0d0, 198.5d0, 201.0d0, 203.5d0, 206.0d0, 208.5d0, 211.1d0, 213.6d0, 216.1d0, 218.6d0,...
            221.1d0, 223.6d0, 226.1d0, 228.6d0, 231.2d0, 233.7d0, 236.2d0, 238.7d0, 241.2d0, 243.7d0,...
            246.2d0, 248.7d0, 251.3d0, 253.8d0, 256.3d0, 258.8d0, 261.3d0, 263.8d0, 266.3d0, 268.8d0,...
            271.4d0, 273.9d0, 276.4d0, 278.9d0, 281.4d0, 283.9d0, 286.4d0, 288.9d0, 291.5d0, 294.0d0,...
            296.5d0, 299.0d0, 301.5d0, 304.0d0, 306.5d0, 309.0d0, 311.6d0, 314.1d0, 316.6d0, 319.1d0,...
            321.6d0, 324.1d0, 326.6d0, 329.1d0, 331.7d0, 334.2d0, 336.7d0, 339.2d0, 341.7d0, 344.2d0,...
            346.7d0, 349.2d0, 351.8d0, 354.3d0, 356.8d0, 359.3d0, 361.8d0, 364.3d0, 366.8d0, 369.3d0,...
            371.9d0, 374.4d0, 376.9d0, 379.4d0, 381.9d0, 384.4d0, 386.9d0, 389.4d0, 392.0d0, 394.5d0,...
            397.0d0, 399.5d0, 402.0d0, 404.5d0, 407.0d0, 409.5d0, 412.1d0, 414.6d0, 417.1d0, 419.6d0,...
            422.1d0, 424.6d0, 427.1d0, 429.6d0, 432.2d0, 434.7d0, 437.2d0, 439.7d0, 442.2d0, 444.7d0,...
            447.2d0, 449.7d0, 452.3d0, 454.8d0, 457.3d0, 459.8d0, 462.3d0, 464.8d0, 467.3d0, 469.8d0,...
            472.4d0, 474.9d0, 477.4d0, 479.9d0, 482.4d0, 484.9d0, 487.4d0, 489.9d0, 492.5d0, 495.0d0, 497.5d0, 500.0d0], ...
            [0.6112d-02,0.6112d-02, 0.6675d0, 0.8394d0, 0.9170d0, 0.9615d0, 0.9904d0, 1.011d0, 1.027d0,...
            1.040d0, 1.051d0, 1.060d0, 1.069d0, 1.077d0, 1.085d0, 1.094d0, 1.102d0, 1.112d0, 1.122d0,...
            1.134d0, 1.149d0, 1.165d0, 1.185d0, 1.208d0, 1.237d0, 1.274d0, 1.302d0, 1.342d0, 1.396d0,...
            1.432d0, 1.483d0, 1.532d0, 1.597d0, 1.667d0, 1.733d0, 1.796d0, 1.865d0, 1.947d0, 2.033d0,...
            2.112d0, 2.182d0, 2.269d0, 2.357d0, 2.442d0, 2.523d0, 2.605d0, 2.688d0, 2.773d0, 2.854d0,...
            2.939d0, 3.024d0, 3.110d0, 3.191d0, 3.263d0, 3.340d0, 3.415d0, 3.490d0, 3.576d0, 3.651d0,...
            3.726d0, 3.794d0, 3.858d0, 3.925d0, 3.993d0, 4.066d0, 4.132d0, 4.201d0, 4.270d0, 4.337d0,...
            4.382d0, 4.444d0, 4.503d0, 4.557d0, 4.611d0, 4.663d0, 4.737d0, 4.792d0, 4.845d0, 4.914d0,...
            4.970d0, 5.024d0, 5.077d0, 5.128d0, 5.169d0, 5.220d0, 5.244d0, 5.290d0, 5.336d0, 5.385d0,...
            5.435d0, 5.485d0, 5.534d0, 5.583d0, 5.626d0, 5.673d0, 5.739d0, 5.781d0, 5.822d0, 5.863d0,...
            5.904d0, 5.943d0, 5.983d0, 6.022d0, 6.061d0, 6.100d0, 6.141d0, 6.176d0, 6.227d0, 6.269d0,...
            6.312d0, 6.353d0, 6.395d0, 6.436d0, 6.476d0, 6.516d0, 6.555d0, 6.592d0, 6.629d0, 6.665d0,...
            6.702d0, 6.739d0, 6.776d0, 6.812d0, 6.851d0, 6.886d0, 6.922d0, 6.957d0, 6.992d0, 7.027d0,...
            7.062d0, 7.098d0, 7.136d0, 7.174d0, 7.211d0, 7.249d0, 7.286d0, 7.322d0, 7.359d0, 7.395d0,...
            7.431d0, 7.467d0, 7.502d0, 7.538d0, 7.573d0, 7.607d0, 7.642d0, 7.678d0, 7.714d0, 7.751d0,...
            7.785d0, 7.818d0, 7.851d0, 7.884d0, 7.918d0, 7.951d0, 7.984d0, 8.017d0, 8.050d0, 8.084d0,...
            8.118d0, 8.152d0, 8.186d0, 8.219d0, 8.253d0, 8.288d0, 8.322d0, 8.357d0, 8.391d0, 8.426d0,...
            8.460d0, 8.493d0, 8.527d0, 8.562d0, 8.596d0, 8.631d0, 8.665d0, 8.700d0, 8.734d0, 8.768d0,...
            8.804d0, 8.837d0, 8.870d0, 8.903d0, 8.936d0, 8.968d0, 9.001d0, 9.033d0, 9.066d0, 9.099d0,...
            9.131d0, 9.163d0, 9.195d0, 9.228d0, 9.260d0, 9.292d0, 9.325d0, 9.357d0, 9.390d0, 9.422d0,...
            9.454d0, 9.487d0]*11604, ...
            theta(:));
end


function r = rr(varargin)
    if nargin == 0
        if rN>1
            r = r1:(r2-r1)/(rN-1):r2;
        else
            r = r1;
        end
    elseif nargin == 1
        ri = varargin{1};
        r = r1+(r2-r1)/(rN-1)*(ri-1);
    else
        error('rr:wrong_args_count','must be o or 1');
    end
end


function i = rindex(r)
    if rN > 1
        i = round(interp1(rr, 1:rN, r));
    else
        i = 1;
    end
end


function z = zz(varargin)
    if nargin == 0
        if zN>1
            z = z1:(z2-z1)/(zN-1):z2;
        else
            z = z1;
        end
    elseif nargin == 1
        zi = varargin{1};
        z = z1+(z2-z1)/(zN-1)*(zi-1);
    else
        error('zz:wrong_args_count','must be o or 1');
    end
end


function i = zindex(z)
    if zN > 1
        i = round(interp1(zz, 1:zN, z));
    else
        i = 1;
    end
end

%{
DOC
\paragraph{Распределение заряда в блине} ...
%}

function electric_factor = calculate_electric_factor
    electric_factor = zeros(zN, rN);

    sigma = a;

    f = @(x,y) 1/(2*pi*sigma*sigma) *exp(- x.*x/(2*sigma^2) - y.*y/(2*sigma^2));

    Rup = @(x,y,z,r,theta) sqrt(((r.*cos(theta)-x).^2 + (r.*sin(theta)-y).^2 + (z-H).^2)).^3;
    Rlow = @(x,y,z,r,theta) sqrt(((r.*cos(theta)-x).^2 + (r.*sin(theta)-y).^2 + (z+H).^2)).^3;

    q = @(r) f(r,0);

    for zi = 1:zN
        for ri = 1:rN
            x = rr(ri);
            y = 0;
            z = zz(zi);

            Exup = integral(@(r) (q(r).*r.*integral(@(theta) ((x-r.*cos(theta))./Rup(x,y,z,r,theta)), 0, 2*pi)), 0, L);
            Eyup = integral(@(r) (q(r).*r.*integral(@(theta) ((y-r.*sin(theta))./Rup(x,y,z,r,theta)), 0, 2*pi)), 0, L);
            Ezup = integral(@(r) (q(r).*r.*integral(@(theta) ((z-H)./Rup(x,y,z,r,theta)), 0, 2*pi)), 0, L);

            Exlow = integral(@(r) (q(r).*r.*integral(@(theta) ((x-r.*cos(theta))./Rlow(x,y,z,r,theta)), 0, 2*pi)), 0, L);
            Eylow = integral(@(r) (q(r).*r.*integral(@(theta) ((y-r.*sin(theta))./Rlow(x,y,z,r,theta)), 0, 2*pi)), 0, L);
            Ezlow = integral(@(r) (q(r).*r.*integral(@(theta) ((z-H)./Rlow(x,y,z,r,theta)), 0, 2*pi)), 0, L);

            electric_factor(zi,ri) = 1/(4*pi*eps_0).*norm([Exup-Exlow, Eyup-Eylow, Ezup-Ezlow]);
        end
    end
end
