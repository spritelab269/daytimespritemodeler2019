% This file is a part of Daytime Sprite Modeler
% Copyright © 2019 IAP RAS, see LICENSE.txt file for EULA

% function value = t1sprite
% t1sprite constant

function value = t1sprite
	value = 50e-3;
end

% autogenerated by reactor revision 'b4615705f07c'
